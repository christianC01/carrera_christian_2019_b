package com.example.apptimerfightertest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.log

class MainActivity : AppCompatActivity() {
    private lateinit var activityTitle: TextView
    private lateinit var gameScoreTextView: TextView
    private lateinit var tapMeButton: Button
    val random_value = List(10) { Random.nextInt(0, 10) }


    // count Down timer cion la ayuda del SO para utilizar el contador

    private lateinit var countDownTimer: CountDownTimer
    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private var timeLeft = 10
    private var gameScore = 0
    private var isGameStarted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        gameScoreTextView = findViewById(R.id.score)
        tapMeButton = findViewById(R.id.tapbtn)

        //playerName =
        //    intent.getStringExtra(MainActivity.INTENT_PLAYER_NAME) ?: "Error getting player name"
        // activityTitle.text = "Get Ready $playerName"
        //activityTitle.text = getString(R.string.get_ready_player, playerName)
        btnstart.setOnClickListener{
            if (!isGameStarted) {
                resetGame()
                startGame()
            }
        }
        tapMeButton.setOnClickListener {
            incrementScore()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "On Destroy called")

    }
    private fun incrementScore() {

        if (!isGameStarted) {
            startGame()
        }
        if (random_value==countDownTimer){
            gameScore+=100
        }else if ((countDownInterval-1)==random_value || (countDownInterval+1)==random_value){
            gameScore+=50
        }
        txtscore.text = getString(R.string., gameScore)

    }

    private fun resetGame() {
        gameScore = 0
        txtscore.text = getString(R.string.score, gameScore)

        timeLeft = 10
        //timeLeftTextView.text = getString(R.string.time_left, timeLeft)

        configCountDownTimer()

        isGameStarted = false

    }

    private fun startGame() {

        countDownTimer.start()
        isGameStarted = true

    }

    private fun endGame() {
        Toast.makeText(this, getString(R.string.game_over, gameScore), Toast.LENGTH_LONG).show()
        resetGame()

    }

    private fun configCountDownTimer(){
        countDownTimer= object : CountDownTimer((timeLeft*1000).toLong(), countDownInterval){
            override fun onFinish() {
                endGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                timeLeft = millisUntilFinished.toInt()/1000
                //timeLeftTextView.text = getString(R.string.time_left, timeLeft)
            }
        }
    }
    companion object { //crear constantes con el nombre de las variables
        private const val SCORE_KEY = "SCORE_KEY"
        private const val TIME_LEFT_KEY = "TIME_LEFT_KEY"
        private const val GAME_STARTED = "GAME_STARTED"
    }
}
